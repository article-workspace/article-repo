-- File create_script.sql updated version 1.6 on 30-Aug-2022
-- change user status column to account_status

USE testdb;

DROP TABLE IF EXISTS report;
DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS post_like;
DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS draft_post;
DROP TABLE IF EXISTS draft_serial;
DROP TABLE IF EXISTS serial;
DROP TABLE IF EXISTS activation;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS enum;
DROP TABLE IF EXISTS user_role;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS user_history;
DROP TABLE IF EXISTS login_audit;
DROP TABLE IF EXISTS user;

CREATE TABLE `user`(
  `user_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email_id` VARCHAR(60) NOT NULL,
  `password` VARCHAR(60) NOT NULL,
  `first_name` VARCHAR(20) NOT NULL,
  `last_name` VARCHAR(20),
  `display_name` VARCHAR(20),
  `photo` VARCHAR(100),
  `about` VARCHAR(250),
  `birth_dt` DATE,
  `street` VARCHAR(40),
  `city` VARCHAR(30),
  `district` VARCHAR(30),
  `state` VARCHAR(30),
  `country` VARCHAR(20),
  `status` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `failed_attempt` TINYINT UNSIGNED DEFAULT 0,
  `created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_email_id_unique` (`email_id` ASC))
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `user_history` (
  `user_history_id` INT UNSIGNED AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `email_id` VARCHAR(60) NOT NULL,
  `password` VARCHAR(60) NOT NULL,
  `first_name` VARCHAR(20) NOT NULL,
  `last_name` VARCHAR(20),
  `display_name` VARCHAR(20),
  `birth_dt` DATE,
  `street` VARCHAR(30),
  `city` VARCHAR(20),
  `district` VARCHAR(20),
  `state` VARCHAR(20),
  `country` VARCHAR(20),
  `status` TINYINT UNSIGNED,
  `failed_attempt` TINYINT UNSIGNED,
  `created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `action` VARCHAR(1),
  PRIMARY KEY (`user_history_id`),
  INDEX `history_user_id_idx` (`user_id` ASC),
  CONSTRAINT `history_user_id_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `role` (
  `role_id` TINYINT UNSIGNED AUTO_INCREMENT,
  `role_name` VARCHAR(20) NOT NULL,
  `role_desc` VARCHAR(100),
  `created_by` INT UNSIGNED NOT NULL,
  `created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`),
  CONSTRAINT `role_created_by_fk`
    FOREIGN KEY (`created_by`)
    REFERENCES `user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `user_role` (
  `user_id` INT UNSIGNED NOT NULL,
  `role_id` TINYINT UNSIGNED NOT NULL,
  `created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  PRIMARY KEY (`user_id`, `role_id`),
  INDEX `user_role_user_id_idx` (`user_id` ASC),
  CONSTRAINT `user_role_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `user_role_role_id`
    FOREIGN KEY (`role_id`)
    REFERENCES `role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `login_audit` (
  `login_audit_id` INT UNSIGNED AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `login_dt` TIMESTAMP NOT NULL,
  `ip_address` VARCHAR(20),
  `browser` VARCHAR(15),
  `is_success` TINYINT NOT NULL,
  `notes` VARCHAR(100),
  `failed_attempt` TINYINT UNSIGNED,
  PRIMARY KEY (`login_audit_id`),
  INDEX `login_audit_id_idx` (`user_id` ASC),
  CONSTRAINT `login_audit_user_id_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `category`	(
	`category_id`	TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`category_name`	VARCHAR(60) NOT NULL,
	`status` TINYINT UNSIGNED NOT NULL DEFAULT 0,
	`created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`category_id`),
	UNIQUE INDEX `category_name_unique` (`category_name` ASC))
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `serial` (
	`serial_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(150) NOT NULL,
	`description` VARCHAR(150) NOT NULL,
	`category_id` TINYINT UNSIGNED NOT NULL,
	`photo_filename` VARCHAR(50),
	`author_id` INT UNSIGNED NOT NULL,
	`status` TINYINT UNSIGNED NOT NULL  DEFAULT 0,
	`created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (serial_id),
	UNIQUE INDEX `serial_title_unique` (`title` ASC),
	CONSTRAINT `serial_author_id_fk`
		FOREIGN KEY (`author_id`)
		REFERENCES `user` (`user_id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT `serial_category_id_fk`
		FOREIGN KEY (`category_id`)
		REFERENCES `category` (`category_id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION)
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `draft_serial` (
	`draft_serial_id` INT NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(150) NOT NULL,
	`description` VARCHAR(150) NOT NULL,
	`category_id` TINYINT UNSIGNED NOT NULL,
	`photo_filename` VARCHAR(50),
	`author_id` INT UNSIGNED NOT NULL,
	`serial_id` INT UNSIGNED NULL,
	`status` TINYINT UNSIGNED NOT NULL  DEFAULT 0,
	`created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`draft_serial_id`),
	UNIQUE INDEX `draft_serial_title_unique` (`title` ASC),
	CONSTRAINT `draft_serial_author_id_fk`
		FOREIGN KEY (`author_id`)
		REFERENCES `user` (`user_id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT `draft_serial_category_id_fk`
		FOREIGN KEY (`category_id`)
		REFERENCES `category` (`category_id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT `draft_serial_serial_id_fk`
	FOREIGN KEY (`serial_id`)
	REFERENCES `serial` (`serial_id`)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION)
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `post` (
  `post_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(250) NOT NULL,
  `content` VARCHAR(15000) NOT NULL,
  `photo_filename` VARCHAR(50),
  `category_id` TINYINT UNSIGNED NOT NULL,
  `serial_id` INT UNSIGNED NULL,
  `tags` VARCHAR(300),
  `author_id` INT UNSIGNED NOT NULL ,
  `views` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  `likes` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `comments` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  `shares` SMALLINT UNSIGNED NOT NULL DEFAULT 1,
  `status` TINYINT UNSIGNED NOT NULL  DEFAULT 0,
  `created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`post_id`),
  UNIQUE INDEX `post_title_unique` (`title` ASC),
  INDEX `post_author_id_idx` (`author_id` ASC),
  FULLTEXT KEY post_fts_idx (title, content, tags),
  CONSTRAINT `post_author_id_fk`
    FOREIGN KEY (`author_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `post_category_id_fk`
    FOREIGN KEY (`category_id`)
    REFERENCES `category` (`category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `post_serial_id_fk`
    FOREIGN KEY (`serial_id`)
    REFERENCES `serial` (`serial_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `draft_post` (
  `draft_post_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(250) NOT NULL,
  `content` VARCHAR(15000) NOT NULL,
  `photo_filename` VARCHAR(50),
  `tags` VARCHAR(300),
  `category_id` TINYINT UNSIGNED NOT NULL,
  `author_id` INT UNSIGNED NOT NULL,
  `serial_id` INT UNSIGNED NULL,
  `post_id` INT UNSIGNED NULL,
  `status` TINYINT UNSIGNED NOT NULL  DEFAULT 0,
  `created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`draft_post_id`),
  UNIQUE INDEX `draft_post_title_unique` (`title` ASC),
  INDEX `draft_post_author_id_idx` (`author_id` ASC),
  FULLTEXT KEY draft_post_fts_idx (title, content, tags),
  CONSTRAINT `draft_post_author_id_fk`
    FOREIGN KEY (`author_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `draft_post_category_id_fk`
    FOREIGN KEY (`category_id`)
    REFERENCES `category` (`category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `draft_post_serial_id_fk`
    FOREIGN KEY (`serial_id`)
    REFERENCES `serial` (`serial_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `comment` (
  `comment_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` INT UNSIGNED NOT NULL,
  `user_id` INT UNSIGNED NOT NULL,
  `comments` VARCHAR(500) NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL  DEFAULT 0,
  `created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`),
  INDEX `comment_post_id_idx` (`post_id` ASC),
  CONSTRAINT `comment_user_id_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `comment_post_id_fk`
    FOREIGN KEY (`post_id`)
    REFERENCES `post` (`post_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `activation` (
  `activation_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `code` VARCHAR(60) NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`activation_id`),
  UNIQUE INDEX `activation_user_id_unique` (`user_id` ASC))
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `report` (
  `report_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `post_id` INT UNSIGNED,
  `comment_id` INT UNSIGNED,
  `reason` VARCHAR(250),
  `resolved_by` INT UNSIGNED,
  `resolved_dt` TIMESTAMP,
  `created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`report_id`),
  CONSTRAINT `report_user_id_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `report_post_id_fk`
    FOREIGN KEY (`post_id`)
    REFERENCES `post` (`post_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `report_comment_id_fk`
    FOREIGN KEY (`comment_id`)
    REFERENCES `comment` (`comment_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `enum`(
  `enum_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `table_name` VARCHAR(15),
  `column_name` VARCHAR(15),
  `enum_key` TINYINT UNSIGNED,
  `enum_value` VARCHAR(30),
  `created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`enum_id`))
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;

CREATE TABLE `post_like` (
	`post_like_id`  INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT UNSIGNED NOT NULL,
	`post_id` INT UNSIGNED NOT NULL,
	`created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
     PRIMARY KEY (`post_like_id`),
	 CONSTRAINT `post_like_post_id_fk`
		FOREIGN KEY (`post_id`)
		REFERENCES `post` (`post_id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	 CONSTRAINT `post_like_user_id_fk`
		FOREIGN KEY (`user_id`)
		REFERENCES `user` (`user_id`)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION)
ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=UTF8MB4_unicode_ci;



INSERT INTO `category`
(category_id, category_name, status)
values
(1, 'பொது', 1),
(2, 'அரசியல்', 1),
(3, 'ஆன்மீகம்', 1), 
(4, 'இசை ', 1),
(5, 'உடல்நலம்', 1), 
(6, 'உணவு', 1),
(7, 'கலை', 1),
(8, 'கல்வி', 1),
(9, 'சமூகம்', 1),
(10, 'சமுதாயம்', 1), 
(11, 'சினிமா', 1),
(12, 'செய்தி', 1), 
(13, 'நலவாழ்வு', 1),
(14, 'பொழுதுபோக்கு', 1), 
(15, 'மனநலம்', 1), 
(16, 'விளையாட்டு', 1);


INSERT INTO `enum`
(table_name, column_name, enum_key, enum_value)
values
('user', 'status', '0', 'ACCOUNT_NOT_ACTIVATED'),
('user', 'status', '1', 'ACCOUNT_ACTIVATED'),
('user', 'status', '2', 'ACCOUNT_LOCKED'),
('draft_post', 'status', '0', 'DRAFTED'),
('draft_post', 'status', '1', 'EDITED'),
('draft_post', 'status', '2', 'APPROVED'),
('draft_post', 'status', '3', 'REJECTED'),
('draft_post', 'status', '4', 'ARCHIVED'),
('draft_post', 'status', '5', 'DELETED'),
('post', 'status', '0', 'INACTIVE'),
('post', 'status', '1', 'ACTIVE'),
('post', 'status', '2', 'ARCHIVED'),
('post', 'status', '3', 'DELETED'),
('draft_serial', 'status', '0', 'DRAFTED'),
('draft_serial', 'status', '1', 'EDITED'),
('draft_serial', 'status', '2', 'APPROVED'),
('draft_serial', 'status', '3', 'REJECTED'),
('draft_serial', 'status', '4', 'ARCHIVED'),
('draft_serial', 'status', '5', 'DELETED'),
('serial', 'status', '0', 'INACTIVE'),
('serial', 'status', '1', 'ACTIVE'),
('serial', 'status', '2', 'ARCHIVED'),
('serial', 'status', '3', 'DELETED');


INSERT INTO `user`
(email_id, password, first_name, last_name, display_name, birth_dt, street, city, district, state, country, status, failed_attempt)
values
('admin@admin.com', '$2b$12$OsStMUZcVhDApN2bGRY16.kd2iQzJc/Fq1np0FzbA9JfzGhkKray2', 'ராமகிருஷ்ணன்', 'பொன்முடி', 'ராம்கி', '1980-01-21','ஒரத்தநாடு','தஞ்சை', 'தஞ்சை','தமிழ்நாடு', 'இந்தியா', 1, 0),
('test@test.com', '$2b$12$OsStMUZcVhDApN2bGRY16.kd2iQzJc/Fq1np0FzbA9JfzGhkKray2', 'ராமகிருஷ்ணன்', 'பொன்முடி', 'ராம்', '1980-01-21','ஒரத்தநாடு','தஞ்சை', 'தஞ்சை','தமிழ்நாடு', 'இந்தியா', 1, 0);



ALTER TABLE draft_post
MODIFY content
varchar(15000);