from flaskapp import ma


class PostListSchema(ma.Schema):
    class Meta:
        fields = ('post_id', 'title', 'content', 'serial_id', 'photo_filename', 'likes', 'category.category_name')


class DraftPostListSchema(ma.Schema):
    class Meta:
        fields = ('draft_post_id', 'title', 'content', 'category.category_name', 'photo_filename')


class DraftSerialListSchema(ma.Schema):
    class Meta:
        fields = ('draft_serial_id', 'title', 'description', 'category.category_name', 'photo_filename')


class SerialListSchema(ma.Schema):
    class Meta:
        fields = ('serial_id', 'title', 'description', 'category.category_name', 'photo_filename', 'total_post')


class DraftPostSchema(ma.Schema):
    class Meta:
        fields = ('draft_post_id', 'title', 'content',  'photo_filename', 'user.display_name', 'user.first_name',
                  'serial_id', 'category_id', 'category.category_name', 'tags', 'updated_dt')


class DraftSerialSchema(ma.Schema):
    class Meta:
        fields = ('draft_serial_id', 'title', 'description',  'photo_filename', 'user.display_name', 'user.first_name',
                  'category_id', 'category.category_name', 'updated_dt')


class PostSchema(ma.Schema):
    class Meta:
        fields = ('post_id', 'title', 'content', 'category_id', 'photo_filename', 'category.category_name', 'tags',
                  'author_id', 'serial_id', 'user.display_name', 'user.first_name', 'created_dt', 'updated_dt',
                  'likes', 'comments', 'shares', 'views')


class DraftPostTitleSchema(ma.Schema):
    class Meta:
        fields = ('draft_post_id', 'title')


class SerialTitleSchema(ma.Schema):
    class Meta:
        fields = ('serial_id', 'title')


class DraftSerialTitleSchema(ma.Schema):
    class Meta:
        fields = ('draft_serial_id', 'title')


class UserSchema(ma.Schema):
    class Meta:
        fields = ('user_id', 'email_id')


class PostActivitySchema(ma.Schema):
    class Meta:
        fields = ('post_id', 'views', 'likes', 'comments', 'shares')


class CommentSchema(ma.Schema):
    class Meta:
        fields = ('comment_id', 'user_id', 'post_id', 'comments', 'created_dt', 'user.display_name', 'user.first_name')


user_schema = UserSchema()
post_schema = PostSchema()
posts_schema = PostListSchema(many=True)
post_activity_schema = PostActivitySchema()
draft_serial_schema = DraftSerialSchema()
draft_serials_schema = DraftSerialSchema(many=True)
draft_post_schema = DraftPostSchema()
draft_posts_schema = DraftPostListSchema(many=True)
draft_posts_title_schema = DraftPostTitleSchema(many=True)
serial_title_schema = SerialTitleSchema(many=True)
draft_serials_title_schema = DraftSerialTitleSchema(many=True)
draft_serials_schema = DraftSerialListSchema(many=True)

serials_schema = SerialListSchema(many=True)
comment_schema = CommentSchema()
comments_schema = CommentSchema(many=True)
