import smtplib
from email.message import EmailMessage
from flaskapp import app

BASE_DOMAIN_WITH_PCL = app.config['BASE_DOMAIN_WITH_PCL']
EMAIL_OUTGOING_SERVER = app.config['EMAIL_OUTGOING_SERVER']
EMAIL_ADDRESS = app.config['EMAIL_ADDRESS']
EMAIL_PASS = app.config['EMAIL_PASS']


def send_test_mail():
    msg = EmailMessage()
    msg['Subject'] = 'New Message 2.0'
    msg['From'] = EMAIL_ADDRESS
    msg['To'] = EMAIL_ADDRESS
    msg.set_content('How about mail reply')
    with smtplib.SMTP(EMAIL_OUTGOING_SERVER, 587) as smtp:
        smtp.starttls()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASS)
        smtp.send_message(msg)
        print('Mail sent successfully...')


def send_activation_link(email_id, first_name, user_id, activation_code):
    message = EmailMessage()
    message['Subject'] = 'Activation Link for மென்னெழுத்து'
    message['From'] = EMAIL_ADDRESS
    message['To'] = email_id
    activation_link = f'{BASE_DOMAIN_WITH_PCL}account_activation?id={user_id}&code={activation_code}'
    content = f'Hi {first_name.capitalize()},\r\n\r\nThanks for register in our website.' \
              f'\r\n\r\nPlease click the below link to activate your account\n{activation_link}'
    message.set_content(content)
    with smtplib.SMTP(EMAIL_OUTGOING_SERVER, 587) as server:
        server.starttls()
        server.login(EMAIL_ADDRESS, EMAIL_PASS)
        server.send_message(message)

