from flaskapp import constants as const

SQL_FETCH_ALL_POST = f"""select post_id, (substring(title, 1, {const.POST_TITLE_DISPLAY_CHAR_CNT})) as title,
                        substring(content, 1, {const.POST_CONTENT_DISPLAY_CHAR_CNT}) as content,
                        c.name category, author_id, likes, p.updated_dt from post p, category c
                        where p.category_id = c.category_id
                        order by post_id desc limit {const.POSTS_DISPLAY_CNT}"""
