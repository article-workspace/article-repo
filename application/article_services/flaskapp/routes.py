import os
import bcrypt
import shutil
from datetime import timedelta
from PIL import Image
from werkzeug.utils import secure_filename
from flask import request, jsonify, make_response, render_template
from flaskapp import app, db, bcrypt, queries, jwt, mail
from flaskapp.models import Post, DraftPost, User, Activation, PostLike, Comment, Category, Serial, DraftSerial
from flaskapp.schemas import *
from flaskapp.enums import PostStatus, DraftPostStatus, DraftSerialStatus, SerialStatus
from flask_restful import Resource, Api, reqparse
from flask_jwt_extended import create_access_token, jwt_required, current_user
from sqlalchemy.exc import IntegrityError, DatabaseError


class SerialFieldListResource(Resource):
    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def get(self):
        schema = request.args.get('schema', None, type=str)
        # alter the query where clause order for performance
        if schema and schema == 'title':
            serials = Serial.query.with_entities(Serial.serial_id, Serial.title)\
                .filter_by(author_id=current_user.user_id)\
                .filter_by(status=SerialStatus.ACTIVE.value)\
                .order_by(Serial.serial_id.desc()).all()
            return serial_title_schema.dump(serials)
        return None


class DraftSerialFieldListResource(Resource):
    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def get(self):
        schema = request.args.get('schema', None, type=str)
        # alter the query where clause order for performance
        if schema and schema == 'title':
            draft_serials = DraftSerial.query.with_entities(DraftSerial.draft_serial_id, DraftSerial.title)\
                .filter_by(author_id=current_user.user_id) \
                .filter(DraftSerial.status.in_((DraftSerialStatus.DRAFTED.value, DraftSerialStatus.EDITED.value))) \
                .order_by(DraftSerial.draft_serial_id.desc()).all()
            return draft_serials_title_schema.dump(draft_serials)
        return None


class DraftPostFieldListResource(Resource):
    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def get(self):
        schema = request.args.get('schema', None, type=str)
        # alter the query where clause order for performance
        draft_posts = DraftPost.query.filter_by(author_id=current_user.user_id)\
            .filter(DraftPost.status.in_((DraftPostStatus.DRAFTED.value, DraftPostStatus.EDITED.value)))\
            .order_by(DraftPost.draft_post_id.desc()).all()
        if schema and schema == 'title':
            return draft_posts_title_schema.dump(draft_posts)
        return draft_posts_schema.dump(draft_posts)


class PostListResource(Resource):
    # noinspection PyMethodMayBeStatic
    def get(self):
        page = request.args.get('page', 1, type=int)
        serial_id = request.args.get('series', None, type=int)
        if serial_id:
            paginated_posts = Post.query.filter_by(status=PostStatus.ACTIVE.value) \
                .filter_by(serial_id=serial_id) \
                .order_by(Post.post_id.asc()) \
                .paginate(page=page, per_page=6, error_out=False)
        else:
            paginated_posts = Post.query.filter_by(status=PostStatus.ACTIVE.value)\
                .order_by(Post.post_id.desc())\
                .paginate(page=page, per_page=6, error_out=False)
        post_items = paginated_posts.items
        db.session.close()
        # Find framework level fix
        for post_item in post_items:
            post_item.title = post_item.title[:75]
            post_item.content = post_item.content[:150]
        return jsonify({
            'success': True,
            'result': posts_schema.dump(post_items),
            'next_num': paginated_posts.next_num,
            'count': len(post_items)
        })


class PostListResourceV1(Resource):
    # noinspection PyMethodMayBeStatic
    def get(self):
        # Implement Flask-Caching implementation to improve performance
        # posts = Post.query.order_by(Post.post_id.desc()).limit(12)
        posts = []
        results = db.engine.execute(queries.SQL_FETCH_ALL_POST)
        for row in results:
            post = Post()
            # post.populate_fields(row._asdict())
            posts.append(post)
        return posts_schema.dump(posts)


class PostResource(Resource):
    # noinspection PyMethodMayBeStatic
    def get(self, post_id):
        post = Post.query.get_or_404(post_id)
        post.views += 1
        db.session.commit()
        return post_schema.dump(post)

    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def patch(self, post_id):
        try:
            post = Post.query.get_or_404(post_id)
            form = request.form
            post.title = form["article_title"]
            post.content = form["article_content"]
            post.category = Category.query.get(form["select_category"])
            post.tags = form["article_tags"]
            file = request.files["article_photo"]
            if file:
                if allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                else:
                    data = {"msg": "file type not accepted"}
                    return make_response(jsonify(data), 415)
            db.session.commit()
            data = {"msg": post.post_id}
            return 204
        except IntegrityError:
            db.session.rollback()
            data = {"msg": "title already exists"}
            return make_response(jsonify(data), 409)

    # noinspection PyMethodMayBeStatic
    def delete(self, post_id):
        post = Post.query.get_or_404(post_id)
        db.session.delete(post)
        db.session.commit()
        return '', 204


class DraftPostListResource(Resource):
    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def get(self):
        page = request.args.get('page', 1, type=int)
        paginated_draft_posts = DraftPost.query.filter_by(author_id=current_user.user_id)\
            .filter(DraftPost.status != DraftPostStatus.APPROVED.value)\
            .order_by(DraftPost.draft_post_id.desc())\
            .paginate(page=page, per_page=3, error_out=False)
        draft_post_items = paginated_draft_posts.items
        db.session.close()
        # Find framework level fix
        for post_item in draft_post_items:
            post_item.title = post_item.title[:100]
            post_item.content = post_item.content[:150]
        return jsonify({
            'success': True,
            'result': draft_posts_schema.dump(draft_post_items),
            'next_num': paginated_draft_posts.next_num,
            'count': len(draft_post_items)
        })

    @jwt_required()
    def post(self):
        try:
            print("start posting draft post form...")
            new_post = DraftPost()
            data = request.form
            new_post.title = data["article_title"]
            new_post.content = data["article_content"]
            new_post.category = Category.query.get(data["select_category"])
            new_post.author_id = current_user.user_id
            article_type = data['choose_article_type']
            if article_type == "series":
                new_post.serial_id = data["select_series"]
            article_tags = data["article_tags"]
            if article_tags:
                new_post.tags = article_tags
            elif article_type == "series":
                new_post.tags = "தொடர்,கட்டுரை"
            else:
                new_post.tags = "கட்டுரை"
            file = request.files["article_photo"]
            upload_folder_path = app.config['UPLOAD_FOLDER']
            print("-------------MAX_CONTENT_LENGTH---------------------------")
            print(app.config['MAX_CONTENT_LENGTH'])
            print("-------------MAX_CONTENT_LENGTH---------------------------")
            if file:
                if allowed_file(file.filename):
                    new_post.photo_filename = file.filename
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(upload_folder_path, "articles", "draft_post", filename))
                else:
                    data = {"msg": "file type not accepted"}
                    return make_response(jsonify(data), 415)
            else:
                new_post.photo_filename = "photo-not-available.jpg"
            db.session.add(new_post)
            db.session.flush()
            db.session.refresh(new_post)
            db.session.commit()
            data = {"draft_post_id": new_post.draft_post_id}
            return make_response(jsonify(data), 201)
        except IntegrityError as ie:
            db.session.rollback()
            # print(str(ie))
            data = {"msg": "title already exists"}
            return make_response(jsonify(data), 409)
        finally:
            print("stop posting draft post form...")


class DraftPostResource(Resource):
    # noinspection PyMethodMayBeStatic
    def get(self, draft_post_id):
        draft_post = DraftPost.query.get_or_404(draft_post_id)
        return draft_post_schema.dump(draft_post)

    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def patch(self, draft_post_id):
        try:
            draft_post = DraftPost.query.get_or_404(draft_post_id)
            form = request.form
            draft_post.title = form["article_title"]
            draft_post.content = form["article_content"]
            article_type = form['choose_article_type']
            if article_type == "series":
                draft_post.serial_id = form["select_series"]
            else:
                draft_post.serial_id = None
            draft_post.category = Category.query.get(form["select_category"])
            draft_post.tags = form["article_tags"]
            draft_post.status = DraftPostStatus.EDITED.value
            file = request.files["article_photo"]
            if file:
                if allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], "articles", "draft_post", filename))
                    draft_post.photo_filename = filename
                else:
                    data = {"msg": "file type not accepted"}
                    return make_response(jsonify(data), 415)
            db.session.commit()
            return 204
        except IntegrityError:
            db.session.rollback()
            data = {"msg": "title already exists"}
            return make_response(jsonify(data), 409)


class SerialListResource(Resource):
    # noinspection PyMethodMayBeStatic
    def get(self):
        page = request.args.get('page', 1, type=int)
        paginated_serials = Serial.query.filter_by(status=SerialStatus.ACTIVE.value).order_by(Serial.serial_id.desc())\
            .paginate(page=page, per_page=6, error_out=False)
        serial_items = paginated_serials.items
        db.session.close()
        # Find framework level fix
        for serial_item in serial_items:
            serial_item.title = serial_item.title[:75]
            serial_item.description = serial_item.description[:150]
            serial_item.total_post = Post.query.filter_by(serial_id=serial_item.serial_id).count()
        return jsonify({
            'success': True,
            'result': serials_schema.dump(serial_items),
            'next_num': paginated_serials.next_num,
            'count': len(serial_items)
        })


class DraftSerialListResource(Resource):
    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def get(self):
        page = request.args.get('page', 1, type=int)
        paginated_draft_serials = DraftSerial.query.filter_by(author_id=current_user.user_id)\
            .filter(DraftSerial.status != DraftSerialStatus.APPROVED.value)\
            .order_by(DraftSerial.draft_serial_id.desc())\
            .paginate(page=page, per_page=3, error_out=False)
        draft_serial_items = paginated_draft_serials.items
        db.session.close()
        # Find framework level fix
        for serial_item in draft_serial_items:
            serial_item.title = serial_item.title[:100]
            serial_item.description = serial_item.description[:150]
        return jsonify({
            'success': True,
            'result': draft_serials_schema.dump(draft_serial_items),
            'next_num': paginated_draft_serials.next_num,
            'count': len(draft_serial_items)
        })

    @jwt_required()
    def post(self):
        try:
            print("start posting draft series form...")
            new_serial = DraftSerial()
            data = request.form
            new_serial.title = data["series_title"]
            new_serial.description = data["series_description"]
            new_serial.category = Category.query.get(data["select_category"])
            new_serial.author_id = current_user.user_id
            file = request.files["series_photo"]
            upload_folder_path = app.config['UPLOAD_FOLDER']
            print("-------------MAX_CONTENT_LENGTH---------------------------")
            print(app.config['MAX_CONTENT_LENGTH'])
            print("-------------MAX_CONTENT_LENGTH---------------------------")
            if file:
                if allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(upload_folder_path, "series", "draft_serial", filename))
                    new_serial.photo_filename = file.filename
                else:
                    data = {"msg": "file type not accepted"}
                    return make_response(jsonify(data), 415)
            else:
                new_serial.photo_filename = "photo-not-available.jpg"
            db.session.add(new_serial)
            db.session.flush()
            db.session.refresh(new_serial)
            db.session.commit()
            data = {"draft_serial_id": new_serial.draft_serial_id}
            return make_response(jsonify(data), 201)
        except IntegrityError as ie:
            db.session.rollback()
            # print(str(ie))
            data = {"msg": "title already exists"}
            return make_response(jsonify(data), 409)
        finally:
            print("stop posting draft series form...")


class DraftSerialResource(Resource):
    # noinspection PyMethodMayBeStatic
    def get(self, draft_serial_id):
        draft_serial = DraftSerial.query.get_or_404(draft_serial_id)
        return draft_serial_schema.dump(draft_serial)

    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def patch(self, draft_serial_id):
        try:
            draft_serial = DraftSerial.query.get_or_404(draft_serial_id)
            form = request.form
            draft_serial.title = form["series_title"]
            draft_serial.description = form["series_description"]
            draft_serial.category = Category.query.get(form["select_category"])
            draft_serial.status = DraftPostStatus.EDITED.value
            file = request.files["series_photo"]
            if file:
                if allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], "series", "draft_serial", filename))
                    draft_serial.photo_filename = filename
                else:
                    data = {"msg": "file type not accepted"}
                    return make_response(jsonify(data), 415)
            db.session.commit()
            return 204
        except IntegrityError:
            db.session.rollback()
            data = {"msg": "title already exists"}
            return make_response(jsonify(data), 409)


class PostActivityResource(Resource):
    # noinspection PyMethodMayBeStatic
    def get(self, post_id):
        post = Post.query.get_or_404(post_id)
        return post_activity_schema.dump(post)


class LoginResource(Resource):
    # noinspection PyMethodMayBeStatic
    def post(self):
        try:
            json_req = request.get_json()
            email = json_req.get("email_id", None)
            password = json_req.get("password", None)
            user = User.query.filter_by(email_id=email).one_or_none()
            if user:
                if user.status == 0:
                    data = {"msg": "account not activated"}
                    return make_response(jsonify(data), 412)
                elif user.status == 2:
                    data = {"msg": "account got locked"}
                    return make_response(jsonify(data), 423)
                elif not bcrypt.check_password_hash(user.password, password):
                    if user.failed_attempt > 5:
                        user.status = 2
                    else:
                        user.failed_attempt += 1
                    db.session.commit()
                    data = {"msg": "invalid username and password combination"}
                    return make_response(jsonify(data), 401)
                else:
                    print("LOGIN SUCCESSFUL")
                    user.failed_attempt = 0
                    db.session.commit()
                    access_token = create_access_token(identity=user, fresh=True, expires_delta=timedelta(minutes=60))
                    data = {"msg": "login successful"}
            else:
                print("LOGIN FAILED")
                data = {"msg": "invalid username and password combination"}
                return make_response(jsonify(data), 401)
        except Exception as e:
            print(e)
            db.session.rollback()
            data = {"msg": "general error occurred"}
            return make_response(jsonify(data), 500)
        return make_response(jsonify(access_token=access_token), 200)


class RegisterResource(Resource):
    # noinspection PyMethodMayBeStatic
    def post(self):
        try:
            validation = True
            if validation:
                new_user = User()
                new_user.populate_fields(request.get_json())
                new_user.password = bcrypt.generate_password_hash(new_user.password).decode('utf-8')
                new_user.status = 0
                db.session.add(new_user)
                db.session.flush()
                db.session.refresh(new_user)

                new_activation = Activation()
                new_activation.user_id = new_user.user_id
                new_activation.status = 0
                new_activation.code = bcrypt.generate_password_hash(new_user.password).decode('utf-8')
                db.session.add(new_activation)

                response = user_schema.dump(new_user)
                json_req = request.get_json();
                mail.send_activation_link(json_req.get("email_id"), json_req.get("first_name"),
                                          new_activation.user_id, new_activation.code)
                db.session.commit()
            else:
                data = {"msg": "input data validation failed"}
                response = make_response(jsonify(data), 400)
        except IntegrityError:
            db.session.rollback()
            data = {"msg": "email exists already"}
            response = make_response(jsonify(data), 409)
        except DatabaseError as de:
            print(de)
            db.session.rollback()
            data = {"msg": "db error occurred"}
            response = make_response(jsonify(data), 500)
        except Exception as e:
            print(e)
            db.session.rollback()
            data = {"msg": "general error occurred"}
            response = make_response(jsonify(data), 500)
        return response


class UserActivationResource(Resource):
    # noinspection PyMethodMayBeStatic
    def get(self):
        print('UserActivationResource executed...')
        success_flag = False;
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('id', type=int, required=True, help='Id cannot be converted')
            parser.add_argument('code', required=True)
            args = parser.parse_args()
            activation = Activation.query.filter_by(user_id=args['id']).one_or_none()

            if activation and activation.code == args['code']:
                user = User.query.filter_by(user_id=args['id']).one_or_none()
                if user:
                    user.status = 1
                    db.session.commit()
                    success_flag = True
        except Exception:
            db.session.rollback()
            success_flag = False
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('activation_confirmation.html', flag=success_flag), 200, headers)


class ProtectedResource(Resource):
    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def get(self):
        print("Who is invoked whois url ----------------------------------------------------------")
        return jsonify(
            user_id=current_user.user_id,
            email_id=current_user.email_id,
            first_name=current_user.first_name,
            display_name=current_user.display_name
        )


class PostLikeResource(Resource):
    @jwt_required()
    def get(self, post_id):
        post_like = PostLike.query.filter_by(user_id=current_user.user_id, post_id=post_id).first()
        if post_like is None:
            data = {"liked": "no"}
        else:
            data = {"liked": "yes"}
        return make_response(jsonify(data), 200)

    @jwt_required()
    def post(self, post_id):
        post_like = PostLike.query.filter_by(user_id=current_user.user_id, post_id=post_id).first()
        if post_like is None:
            like = PostLike(user_id=current_user.user_id, post_id=post_id)
            post = Post.query.get_or_404(post_id)
            post.likes += 1
            db.session.add(like)
            db.session.commit()
            data = {"liked": "yes"}
            return make_response(jsonify(data), 201)
        else:
            # return '', 409  # conflict
            return self.delete(post_id)

    @jwt_required()
    def delete(self, post_id):
        post_like = PostLike.query.filter_by(user_id=current_user.user_id, post_id=post_id).first()
        if post_like is not None:
            post = Post.query.get_or_404(post_id)
            post.likes -= 1
            likes_count = post.likes
            db.session.delete(post_like)
            db.session.commit()
            data = {"liked": "no"}
            print("liked : no")
            return make_response(jsonify(data), 200)  # 204 if no content
        else:
            return '', 404


class SharePostResource(Resource):
    # noinspection PyMethodMayBeStatic
    def patch(self, post_id):
        post = Post.query.get_or_404(post_id)
        post.shares += 1
        db.session.commit()
        return 204


class CommentResource(Resource):
    # noinspection PyMethodMayBeStatic
    def get(self, comment_id):
        comment = Comment.query.get_or_404(comment_id)
        db.session.commit()
        return comment_schema.dump(comment)

    # noinspection PyMethodMayBeStatic
    def patch(self, comment_id):
        comment = Comment.query.get_or_404(comment_id)
        comment.update_fields(request.get_json())
        db.session.commit()
        return comment_schema.dump(comment)

    # noinspection PyMethodMayBeStatic
    def delete(self, comment_id):
        comment = Comment.query.get_or_404(comment_id)
        db.session.delete(comment)
        db.session.commit()
        return '', 204


class CommentListResource(Resource):
    # noinspection PyMethodMayBeStatic
    def get(self, post_id):
        page = request.args.get('page', 1, type=int)
        # Implement Flask-Caching implementation to improve performance
        comments = Comment.query.filter_by(post_id=post_id).order_by(Comment.comment_id.asc()).paginate(
            page=page, per_page=5, error_out=False)
        paginated_comments = comments.items
        return jsonify({
            'success': True,
            'result': comments_schema.dump(paginated_comments),
            'next_num': comments.next_num,
            'count': len(paginated_comments)
        })

    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def post(self, post_id):
        # print("Inside ADD Comment section")
        comments = request.get_json().get("comment", None)
        comment = Comment(current_user.user_id, post_id, comments)
        db.session.add(comment)
        db.session.flush()
        db.session.refresh(comment)
        post = Post.query.get_or_404(post_id)
        post.comments += 1
        db.session.commit()
        return comment_schema.dump(comment), 201


class ApproveDraftPostResource(Resource):
    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def post(self):
        try:
            draft_post_id = request.get_json().get("draft_post_id", None)
            draft_post = DraftPost.query.get(draft_post_id)
            new_post = Post()
            new_post.title = draft_post.title
            new_post.content = draft_post.content
            new_post.photo_filename = draft_post.photo_filename
            new_post.tags = draft_post.tags
            new_post.category_id = draft_post.category_id
            new_post.author_id = draft_post.author_id
            new_post.status = PostStatus.ACTIVE.value
            new_post.serial_id = draft_post.serial_id
            db.session.add(new_post)
            db.session.flush()
            db.session.refresh(new_post)
            draft_post.post_id = new_post.post_id
            draft_post.status = DraftPostStatus.APPROVED.value
            db.session.commit()
            try:
                upload_folder_path = app.config['UPLOAD_FOLDER']
                src_file = os.path.join(upload_folder_path, "articles", "draft_post", draft_post.photo_filename)
                out_file = os.path.join(upload_folder_path, "articles", "post", draft_post.photo_filename)
                with open(src_file, "rb") as file:
                    output_size = (1920, 1080)
                    image = Image.open(file)
                    image.thumbnail(output_size)
                    image.save(out_file)
            except IOError as ioe:
                print(ioe)
            data = {"new_post_id": new_post.post_id}
            return make_response(jsonify(data), 201)
        except IntegrityError:
            db.session.rollback()
            data = {"msg": "title already exists"}
            return make_response(jsonify(data), 409)


class ApproveDraftSerialResource(Resource):
    # noinspection PyMethodMayBeStatic
    @jwt_required()
    def post(self):
        try:
            draft_serial_id = request.get_json().get("draft_serial_id", None)
            draft_serial = DraftSerial.query.get(draft_serial_id)
            new_serial = Serial()
            new_serial.title = draft_serial.title
            new_serial.description = draft_serial.description
            new_serial.category_id = draft_serial.category_id
            new_serial.photo_filename = draft_serial.photo_filename
            new_serial.author_id = draft_serial.author_id
            new_serial.status = SerialStatus.ACTIVE.value
            db.session.add(new_serial)
            db.session.flush()
            db.session.refresh(new_serial)
            draft_serial.serial_id = new_serial.serial_id
            draft_serial.status = DraftSerialStatus.APPROVED.value
            db.session.commit()
            try:
                upload_folder_path = app.config['UPLOAD_FOLDER']
                src_file = os.path.join(upload_folder_path, "series", "draft_serial", draft_serial.photo_filename)
                out_file = os.path.join(upload_folder_path, "series", "serial", draft_serial.photo_filename)
                with open(src_file, "rb") as file:
                    output_size = (1920, 1080)
                    image = Image.open(file)
                    image.thumbnail(output_size)
                    image.save(out_file)
            except IOError as ioe:
                print(ioe)
            data = {"new_serial_id": new_serial.serial_id}
            return make_response(jsonify(data), 201)
        except IntegrityError:
            db.session.rollback()
            data = {"msg": "title already exists"}
            return make_response(jsonify(data), 409)

        
@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return User.query.filter_by(user_id=identity).one_or_none()


@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.user_id


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in {'png', 'jpg', 'jpeg'}


api = Api(app)

# USER
api.add_resource(LoginResource, '/login')
api.add_resource(RegisterResource, '/register')
api.add_resource(ProtectedResource, '/who_is')
api.add_resource(UserActivationResource, '/account_activation')
# DRAFT_POST
api.add_resource(DraftPostListResource, '/draft_posts')
api.add_resource(DraftPostFieldListResource, '/draft_posts/fields')
api.add_resource(DraftPostResource, '/draft_posts/<int:draft_post_id>')
api.add_resource(ApproveDraftPostResource, '/approve_draft_post')
# POST
api.add_resource(PostListResource, '/posts')
api.add_resource(PostResource, '/posts/<int:post_id>')
# POST_RELATED
api.add_resource(PostLikeResource, '/like/<int:post_id>')
api.add_resource(PostActivityResource, '/activity/<int:post_id>')
api.add_resource(CommentListResource, '/posts/<int:post_id>/comments')
# DRAFT_SERIES
api.add_resource(DraftSerialListResource, '/draft_serials')
api.add_resource(DraftSerialFieldListResource, '/draft_serials/fields')
api.add_resource(DraftSerialResource, '/draft_serials/<int:draft_serial_id>')
api.add_resource(ApproveDraftSerialResource, '/approve_draft_serial')
# SERIES
api.add_resource(SerialListResource, '/serials')
api.add_resource(SerialFieldListResource, '/serials/fields')








