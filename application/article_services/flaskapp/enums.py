from enum import IntEnum


class PostStatus(IntEnum):
    INACTIVE = 0
    ACTIVE = 1
    ARCHIVED = 2
    DELETED = 3


class DraftPostStatus(IntEnum):
    DRAFTED = 0
    EDITED = 1
    APPROVED = 2
    REJECTED = 3
    ARCHIVED = 4
    DELETED = 5


class UserAccountStatus(IntEnum):
    ACCOUNT_NOT_ACTIVATED = 0
    ACCOUNT_ACTIVATED = 1
    ACCOUNT_LOCKED = 2


class SerialStatus(IntEnum):
    INACTIVE = 0
    ACTIVE = 1
    ARCHIVED = 2
    DELETED = 3


class DraftSerialStatus(IntEnum):
    DRAFTED = 0
    EDITED = 1
    APPROVED = 2
    REJECTED = 3
    ARCHIVED = 4
    DELETED = 5

