from flaskapp import db
from sqlalchemy import text
from datetime import datetime
from sqlalchemy.dialects.mysql import SMALLINT, TINYINT, INTEGER, MEDIUMINT


class User(db.Model):
    __tablename__ = 'user'
    user_id = db.Column(INTEGER(unsigned=True), nullable=False, primary_key=True)
    email_id = db.Column(db.String(60), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    first_name = db.Column(db.String(20), nullable=False)
    last_name = db.Column(db.String(20), nullable=True)
    display_name = db.Column(db.String(20), nullable=True)
    birth_dt = db.Column(db.DateTime(timezone=True), nullable=True)
    street = db.Column(db.String(40), nullable=True)
    city = db.Column(db.String(30), nullable=True)
    district = db.Column(db.String(30), nullable=True)
    state = db.Column(db.String(30), nullable=True)
    country = db.Column(db.String(20), nullable=True)
    status = db.Column(TINYINT(unsigned=True), server_default=text('0'), nullable=False)
    failed_attempt = db.Column(TINYINT(unsigned=True), nullable=True, server_default=text('0'))
    created_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'))
    updated_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'), onupdate=datetime.utcnow())
    posts = db.relationship('Post', backref='user', lazy=True)
    draft_posts = db.relationship('DraftPost', backref='user', lazy=True)
    # P->C lazy while fetching children, C->P eager while fetching parent
    comments = db.relationship('Comment', backref=db.backref('user', lazy='joined'), lazy=True)

    def populate_fields(self, dictionary):
        for name, value in dictionary.items():
            setattr(self, name, value)

    def __repr__(self):
        return f'<User {self.email_id}>'


class Activation(db.Model):
    __tablename__ = 'activation'
    activation_id = db.Column(INTEGER(unsigned=True), nullable=False, primary_key=True)
    user_id = db.Column(INTEGER(unsigned=True), unique=True, nullable=False)
    code = db.Column(db.String(60), nullable=False)
    status = db.Column(TINYINT(unsigned=True), server_default=text('0'), nullable=False)
    created_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'))
    updated_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'), onupdate=datetime.utcnow())

    def populate_fields(self, dictionary):
        for name, value in dictionary.items():
            setattr(self, name, value)

    def __repr__(self):
        return f'<Activation {self.activation_id}>'


class Post(db.Model):
    __tablename__ = 'post'
    post_id = db.Column(INTEGER(unsigned=True), nullable=False, primary_key=True)
    title = db.Column(db.String(250), unique=True, nullable=False)
    content = db.Column(db.String(15000), nullable=False)
    photo_filename = db.Column(db.String(50), nullable=True)
    category_id = db.Column(TINYINT(unsigned=True), db.ForeignKey('category.category_id'), nullable=False)
    serial_id = db.Column(INTEGER(unsigned=True), db.ForeignKey('serial.serial_id'), nullable=True)
    tags = db.Column(db.String(300), nullable=True)
    author_id = db.Column(INTEGER(unsigned=True), db.ForeignKey('user.user_id'), nullable=False)
    views = db.Column(MEDIUMINT(unsigned=True), server_default=text('0'), nullable=False)
    likes = db.Column(SMALLINT(unsigned=True), server_default=text('0'), nullable=False)
    comments = db.Column(SMALLINT(unsigned=True), server_default=text('0'), nullable=False)
    shares = db.Column(SMALLINT(unsigned=True), server_default=text('1'), nullable=False)
    status = db.Column(TINYINT(unsigned=True), server_default=text('0'), nullable=False)
    created_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'))
    updated_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'), onupdate=datetime.utcnow())
    category = db.relationship('Category', back_populates='posts', lazy=False)
    serial = db.relationship('Serial', back_populates='posts', lazy=True)

    def populate_fields(self, dictionary):
        for name, value in dictionary.items():
            setattr(self, name, value)

    def __repr__(self):
        return f'<Post {self.post_id}>'


class DraftPost(db.Model):
    __tablename__ = 'draft_post'
    draft_post_id = db.Column(INTEGER(unsigned=True), nullable=False, primary_key=True)
    title = db.Column(db.String(250), unique=True, nullable=False)
    content = db.Column(db.String(15000), nullable=False)
    photo_filename = db.Column(db.String(50), nullable=True)
    tags = db.Column(db.String(300), nullable=True)
    category_id = db.Column(TINYINT(unsigned=True), db.ForeignKey('category.category_id'), nullable=False)
    author_id = db.Column(INTEGER(unsigned=True), db.ForeignKey('user.user_id'), nullable=False)
    serial_id = db.Column(INTEGER(unsigned=True), db.ForeignKey('serial.serial_id'), nullable=True)
    post_id = db.Column(INTEGER(unsigned=True), nullable=True)
    status = db.Column(TINYINT(unsigned=True), server_default=text('0'), nullable=False)
    created_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'))
    updated_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'), onupdate=datetime.utcnow())
    category = db.relationship('Category', back_populates='draft_posts', lazy=False)

    def populate_fields(self, dictionary):
        for name, value in dictionary.items():
            setattr(self, name, value)

    def __repr__(self):
        return f'<Post {self.post_id}>'


class Category(db.Model):
    __tablename__ = 'category'
    category_id = db.Column(TINYINT(unsigned=True), nullable=False, primary_key=True)
    category_name = db.Column(db.String(60), nullable=False)
    status = db.Column(TINYINT(unsigned=True), server_default=text('0'), nullable=False)
    created_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'))
    updated_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'), onupdate=datetime.utcnow())
    posts = db.relationship('Post', back_populates='category')
    draft_posts = db.relationship('DraftPost', back_populates='category')
    serials = db.relationship('Serial', back_populates='category')
    draft_serials = db.relationship('DraftSerial', back_populates='category')

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f'<Category {self.name} | {self.active}>'


class PostLike(db.Model):
    __tablename__ = 'post_like'
    post_like_id = db.Column(INTEGER(unsigned=True), nullable=False, primary_key=True)
    user_id = db.Column(INTEGER(unsigned=True), db.ForeignKey('user.user_id'), nullable=False)
    post_id = db.Column(INTEGER(unsigned=True), db.ForeignKey('post.post_id'), nullable=False)
    created_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'))

    def __init__(self, user_id, post_id):
        self.user_id = user_id
        self.post_id = post_id

    def __repr__(self):
        return f'<PostLike {self.post_id} | {self.user_id}>'


class Comment(db.Model):
    __tablename__ = 'comment'
    comment_id = db.Column(INTEGER(unsigned=True), nullable=False, primary_key=True)
    user_id = db.Column(INTEGER(unsigned=True), db.ForeignKey('user.user_id'), nullable=False)
    post_id = db.Column(INTEGER(unsigned=True), db.ForeignKey('post.post_id'), nullable=False)
    comments = db.Column(db.String(500), nullable=False)
    status = db.Column(TINYINT(unsigned=True), server_default=text('0'), nullable=False)
    created_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'))
    updated_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'), onupdate=datetime.utcnow())

    def __init__(self, user_id, post_id, comments):
        self.user_id = user_id
        self.post_id = post_id
        self.comments = comments

    def populate_fields(self, dictionary):
        for name, value in dictionary.items():
            setattr(self, name, value)

    def __repr__(self):
        return f'<Comment {self.post_id} | {self.user_id}>'


class Serial(db.Model):
    __tablename__ = 'serial'
    serial_id = db.Column(INTEGER(unsigned=True), nullable=False, primary_key=True)
    title = db.Column(db.String(150), unique=True, nullable=False)
    description = db.Column(db.String(150), nullable=False)
    category_id = db.Column(TINYINT(unsigned=True), db.ForeignKey('category.category_id'), nullable=False)
    photo_filename = db.Column(db.String(50), nullable=True)
    author_id = db.Column(INTEGER(unsigned=True), db.ForeignKey('user.user_id'), nullable=False)
    status = db.Column(TINYINT(unsigned=True), server_default=text('0'), nullable=False)
    created_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'))
    updated_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'), onupdate=text('(UTC_TIMESTAMP)'))
    category = db.relationship('Category', back_populates='serials', lazy=False)
    posts = db.relationship('Post', back_populates='serial', lazy=True)

    def populate_fields(self, dictionary):
        for name, value in dictionary.items():
            setattr(self, name, value)

    def __repr__(self):
        return f'<Serial {self.serial_id}>'


class DraftSerial(db.Model):
    __tablename__ = 'draft_serial'
    draft_serial_id = db.Column(INTEGER(unsigned=True), nullable=False, primary_key=True)
    title = db.Column(db.String(150), unique=True, nullable=False)
    description = db.Column(db.String(150), nullable=False)
    category_id = db.Column(TINYINT(unsigned=True), db.ForeignKey('category.category_id'), nullable=False)
    photo_filename = db.Column(db.String(50), nullable=True)
    author_id = db.Column(INTEGER(unsigned=True), db.ForeignKey('user.user_id'), nullable=False)
    serial_id = db.Column(INTEGER(unsigned=True), nullable=True)
    status = db.Column(TINYINT(unsigned=True), server_default=text('0'), nullable=False)
    created_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'))
    updated_dt = db.Column(db.DateTime, nullable=False, default=datetime.utcnow(), server_default=text('(UTC_TIMESTAMP)'), onupdate=datetime.utcnow())
    category = db.relationship('Category', back_populates='draft_serials', lazy=False)

    def populate_fields(self, dictionary):
        for name, value in dictionary.items():
            setattr(self, name, value)

    def __repr__(self):
        return f'<DraftSerial {self.draft_serial_id}>'