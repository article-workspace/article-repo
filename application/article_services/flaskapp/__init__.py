from flask import Flask
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow


app = Flask(__name__, instance_relative_config=True)
app.debug = True

# v1_cors_config = {
#    "origins": ["http://127.0.0.1:5500"],
#    "methods": ["GET", "POST", "OPTIONS", "HEAD"],
#    "allow_headers": ["Origin", "Authorization", "Content-Type", "Accept", "Access-Control-Allow-Origin",
#                      "Access-Control-Allow-Headers"],
#    "supports_credentials": True
#}

v2_cors_config = {
    "origins": ["http://127.0.0.1:5500"],
    "supports_credentials": True
}

CORS(app, resources={r"/*": v2_cors_config})
app.config.from_object('config')
app.config.from_pyfile('local_config.py')
# maximum file payload limit 2MB
app.config['MAX_CONTENT_LENGTH'] = 2 * 1024 * 1024
bcrypt = Bcrypt(app)
jwt = JWTManager(app)
db = SQLAlchemy(app)
ma = Marshmallow(app)

from flaskapp import routes

# db.create_all()
